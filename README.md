# MengRan
项目 介绍
梦染后台管理系统，开发工具使用的Visual Studio 2019，使用layui框架.mysql数据库.梦染聊天室使用的signalr实时通讯技术，NET.CORE版本为5.0。使用语言：C#.Html.Js.Css。数据主要来源数据库和接口，目前项目还在开发中，敬请期待跟多的功能。

#### 软件架构
开发工具使用的Visual Studio 2019，使用layui框架.mysql数据库.signalr实时通讯技术，使用NET.CORE版本为5.0。使用语言：C#.Html.Js.Css。数据主要来源数据库和接口，目前项目还在开发中，敬请期待跟多的功能

#### 项目演示
![梦染登录页](wwwroot/images/1.png)
![天气查询，通过调用天气接口查询未来5天的天气](wwwroot/images/2.png)
![登录日志](wwwroot/images/3.png)
![梦染聊天室](wwwroot/images/4.png)
![账号管理](wwwroot/images/5.png)

#### 安装教程

1.  拉取之后，新建一个mengran库，然后吧脚本复制进去跑一边，在use表中添加一条登录的数据来登录后台用


#### signalr使用说明

1.  项目做配备的聊天可以进行群聊，
2.  梦染聊天室集成kindeditor控件，完成了发送图片和发送表情，可以在在线列表中查看在线的用户，
3.  发送的内容可以在聊天记录模块中查看聊天室的消息记录



