﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.yuanshen
{
    public class choukashuju
    {
        public int uid { get; set; }
        public int gacha_type { get; set; }
        public string item_id { get; set; }
        public int count { get; set; }
        public string time { get; set; }
        public string name { get; set; }
        public string lang { get; set; }
        public string item_type { get; set; }
        public int rank_type { get; set; }
        public string id { get; set; }
    }
}
