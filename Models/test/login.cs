﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.test
{
    [Table("logins")]
    public class login
    {
        [Key]//设置主键
        public int id { get; set; }
        public string ip { get; set; }
        public string ipweizhi { get; set; }

        public string browser { get; set; }
        public string cid { get; set; }
        public DateTime logintime { get; set; }
    }
}
