﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.test
{
    public class forecast
    {
        public string date { get; set; }
        public string high { get; set; }
        public string fx { get; set; }

        public string low { get; set; }
        public string fl { get; set; }
        public string type { get; set; }
    }
}
