﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.bili
{
    [Table("bilimes")]
    public class qinglun
    {
        [Key]
        public int id { get; set; }
        //啊B-id
        public string mid { get; set; }
        //用户名
        public string uname { get; set; }
        //点赞数量
        public int like { get; set; }
        //评论内容
        public string message { get; set; }
        //评论时间
        public int ctime { get; set; }
        //评论回复数量
        public int rcount { get; set; }
    }
    public class replies
    {
        public int rcount { get; set; }
        public int like { get; set; }
        public int ctime { get; set; }
        public member member { get; set; }
        public content content { get; set; }
    }
    public class member
    {
        public string mid { get; set; }
        public string uname { get; set; }
    }
    public class content
    {
        public string message { get; set; }
    }
}
