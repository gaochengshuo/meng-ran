﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.index
{
    [Table("mens")]
    public class mens
    {
        [Key]
        public int id { get; set; }
        public string datatitle { get; set; }
        public int dataid { get; set; }
        public string classa { get; set; }
        public string layhref { get; set; }
        public string dataname { get; set; }
        public int zhuci { get; set; }
    }
}
