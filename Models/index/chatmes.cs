﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.index
{
    [Table("chatmess")]
    public class chatmes
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string mess { get; set; }
        public DateTime datatime { get; set; }
    }
}
