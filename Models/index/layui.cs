﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.index
{
    public class layui
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string sex { get; set; }
        public string city { get; set; }
        public string sign { get; set; }
        public int experience { get; set; }
        public string ip { get; set; }
        public int logins { get; set; }
        public DateTime joinTime { get; set; }
      

    }
}
