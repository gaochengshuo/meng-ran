﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.index
{
    [Table("use")]
    public class user
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public DateTime settime { get; set; }
    }
}
