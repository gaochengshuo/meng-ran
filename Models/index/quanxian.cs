﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models.index
{
    [Table("menus")]
    public class quanxian
    {
        public int id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string lcon { get; set; }
        public int datais { get; set; }
    }
}
