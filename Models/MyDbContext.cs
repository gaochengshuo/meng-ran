﻿
using MengRan.Models.bili;
using MengRan.Models.index;
using MengRan.Models.test;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Models
{
    public class MyDbContext : DbContext
    {
        //public DbSet<Message> Chatmes { get; set; }
        public DbSet<userr> use { get; set; }
        public DbSet<login> logins { get; set; }
        public DbSet<chatmes> chatmess { get; set; }
        public DbSet<qinglun> bilimes { get; set; }
        public DbSet<mens> men { get; set; }
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }
    }
}
