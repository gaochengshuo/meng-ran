﻿
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Socket
{
    /// <summary>
    /// 用户实体类
    /// </summary>
    public class User
    {
        /// <summary>
        /// 连接ID
        /// </summary>
        [Key]
        public string ConnectionID { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name { get; set; }

        public User(string name, string connectionId)
        {
            this.Name = name;
            this.ConnectionID = connectionId;
        }
    }
    public class ChatHub : Hub
    {
        public static List<User> users = new List<User>();

        //发送所有
        public async Task SendMessage(string name, string message)
        {

            DateTime time = DateTime.Now;
            await Clients.All.SendAsync("ReceiveMessage", name, message, time);
        }
        //发送用户列表
        public async Task GetName(string uer)
        {
            //获取用户名，发送至用户列表
            var user = users.SingleOrDefault(u => u.ConnectionID == Context.ConnectionId);
            if (user != null)
            {
                user.Name = uer;

            }
            //获取所有在线用户
            var list = users.Select(s => new { s.Name, s.ConnectionID }).ToList();
            for (var i = 0; i < list.Count; i++)
            {
                await Clients.All.SendAsync("Connected", list[i].Name, i, list[i].ConnectionID);
            }
        }
        /// <summary>
        /// 用户连接时调用
        /// </summary>
        public override async Task OnConnectedAsync()
        {
            var user = users.Where(u => u.ConnectionID == Context.ConnectionId).SingleOrDefault();
            //判断用户是否存在，否则添加集合
            if (user == null)
            {
                user = new User("", Context.ConnectionId);
                users.Add(user);
            }

            await Clients.All.SendAsync("1");

        }
        /// <summary>
        /// 用户断开时调用
        /// </summary>
        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var user = users.Where(p => p.ConnectionID == Context.ConnectionId).FirstOrDefault();
            //判断用户是否存在，存在则删除
            if (user != null)
            {
                //删除用户
                users.Remove(user);
            }
            //获取所有在线用户
            var list = users.Select(s => new { s.Name, s.ConnectionID }).ToList();
            for (var i = 0; i < list.Count; i++)
            {
                await Clients.All.SendAsync("Connected", list[i].Name, i);
            }

        }
        //获取所有用户在线列表
        private void GetUsers()
        {
            var list = users.Select(s => new { s.Name, s.ConnectionID }).ToList();
            string jsonList = JsonConvert.SerializeObject(list);
            Clients.All.SendAsync("getUsers", jsonList);
        }
        //一对一私聊方法
        //
        public async Task SendToUser(string name, string res, string message)
        {
            await Clients.Clients(res, Context.ConnectionId).SendAsync("ReceiveMessage", name, message);
        }
    }
}