﻿using MengRan.Models;
using MengRan.Models.index;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Controllers.api
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class yonghuController : Controller
    {
        private readonly MyDbContext _dbContext;
        public yonghuController(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public IActionResult Del(int ids)
        {
            if (ids==1)
            { 
                return Json(new { code = 0 });
            }
            if (ids ==0)
            {
                return Json(new { code = 0 });
            }

            var cs = _dbContext.use.Single(x => x.id == ids);
            _dbContext.Remove(cs);
            var count = _dbContext.SaveChanges();
            if (count > 0) 
            {
                return Json(new { code = 200, Message = "删除成功" });
            }

            else
            {
                return Json(new { code = 0, Message = "删除失败" });
            }

        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public IActionResult xiugai(int ids,string pad)
        {
           
            var cs = _dbContext.use.FirstOrDefault(x => x.id == ids);
            cs.password = pad;
            _dbContext.SaveChanges();
            return Json(new { code = 200, Message = "修改成功" });
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [HttpGet]
        public ActionResult adds(string name, string pad)
        {
            using (_dbContext)
            {
                var ad = new userr
                {
                    username = name,
                    password = pad,
                    settime = DateTime.Now
                };
                _dbContext.use.Add(ad);
                var a = _dbContext.SaveChanges();

                if (a > 0)
                {
                    return Json(new { cood = 200, Message = "添加成功" });
                }
                else
                {
                    return Json(new { cood = 0, Message = "添加失败" });
                }
            }
        }
        [HttpGet]
        public ActionResult update(string name, string pad)
        {
            using (_dbContext)
            {
                var ad = new userr
                {
                    username = name,
                    password = pad,
                    settime = DateTime.Now
                };
                _dbContext.use.Update(ad);
                var a = _dbContext.SaveChanges();

                if (a > 0)
                {
                    return Json(new { cood = 200, Message = "修改成功" });
                }
                else
                {
                    return Json(new { cood = 0, Message = "修改失败" });
                }
            }
        }
        public ActionResult mens()
        {
            var men = _dbContext.men.ToList();
            return Json(new { cood = 0, count = men.Count, data = men, mes = "" });
        }
    }
}
