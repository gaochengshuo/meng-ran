﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MengRan.Models;
using MengRan.Models.index;
using MengRan.Models.test;
using MengRan.Models.yuanshen;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MengRan.Controllers.api
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class liuyanController : Controller
    {
        private readonly MyDbContext _dbContext;
        public liuyanController(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        [HttpGet]
        public IActionResult liuyan()
        {
            string ip = "http://124.222.78.73:8080/api/people/getall";


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ip);
            WebResponse resp = req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.Default);
            string sReturn = sr.ReadToEnd().Trim();
            List<liuyanmes> jsonq = JsonConvert.DeserializeObject<List<liuyanmes>>(sReturn);

            return Ok(new { code = 0, count = jsonq.Count, data = jsonq, mes = "" });

        }
        [HttpPost]
        public static string HttpPost(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url); request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            Encoding encoding = Encoding.UTF8;
            byte[] postData = encoding.GetBytes(postDataStr);
            request.ContentLength = postData.Length;
            Stream myRequestStream = request.GetRequestStream(); myRequestStream.Write(postData, 0, postData.Length);
            myRequestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, encoding); string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();
            return retString;
        }
        [HttpGet]
        public IActionResult Zeng(string name, string pad)
        {
            var a = "";
            using (_dbContext)
            {
                var us = new userr
                {
                    username = name,
                    password = pad,
                    settime = DateTime.Now
                };
                _dbContext.use.Add(us);
                var i = _dbContext.SaveChanges();
                a = i > 0 ? "数据写入成功" : "数据写入失败";
            }
            return Ok(a);
        }
       
        [HttpGet]
        public IActionResult setall()
        {
            using (_dbContext)
            {
                var list = _dbContext.use.ToList();
                var a = list.Count;
                return Ok(new { code = 0, count = a, data = list, mes = "" });
            }
        }
        [HttpGet]
        public IActionResult layui()
        {
            string ip = "https://layui.itze.cn//test/table/demo1.json";


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ip);
            WebResponse resp = req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.Default);
            string sReturn = sr.ReadToEnd().Trim();

            JObject result = JObject.Parse(sReturn);
            string message = result.Value<JToken>("data").ToString();


            //    string data = result.ToString();
            //    var datas = "[" + data.TrimStart("{".ToCharArray()).TrimEnd("}".ToCharArray()) + "]";
            List<layui> jsonq = JsonConvert.DeserializeObject<List<layui>>(message);

            return Ok(new { code = 0, count = jsonq.Count, data = jsonq, mes = "" });

        }
        //[HttpGet]
        //public IActionResult chatmes()
        //{

        //    string ip = "http://124.222.78.73:8082/api/chat/getall";

        //    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ip);
        //    WebResponse resp = req.GetResponse();
        //    StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.Default);
        //    string sReturn = sr.ReadToEnd().Trim();
        //    List<Models.index.chatmes> jsonq = JsonConvert.DeserializeObject<List<Models.index.chatmes>>(sReturn);
        //    return Ok(new { code = 0, count = jsonq.Count, data = jsonq, mes = "" });
        //}
        [HttpGet]
        public IActionResult chatmes()
        {

            using (_dbContext)
            {
                var list = _dbContext.chatmess.OrderByDescending(b => b.id).ToList();
                return Ok(new { code = 0, count = list.Count, data = list, mes = "" });
            }
        }
        [HttpGet]
        public IActionResult biili()
        {

            using (_dbContext)
            {
                var list = _dbContext.bilimes.ToList();
                return Ok(new { code = 0, count = list.Count, data = list, mes = "" });
            }
        }


        [HttpGet]
        public IActionResult delete()
        {

            return Ok();
        }
        [HttpPost]
        public IActionResult menss()
        {
            using (_dbContext)
            {
                var list = _dbContext.men.ToList();
                return Ok(list);
            }
           
        }
        [HttpGet]
        public string Create(string ip, string cid, string ipweizhi, string browser)
        {
            var message = "";
            using (_dbContext)
            {
                var a = new login
                {
                    ip = ip,
                    cid = cid,
                    ipweizhi = ipweizhi,
                    browser = browser,
                    logintime = DateTime.Now
                };
                _dbContext.logins.Add(a);
                var i = _dbContext.SaveChanges();
                message = i > 0 ? "添加成功" : "添加失败";
            }
            return message;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            using (_dbContext)
            {
                var list = _dbContext.logins.OrderByDescending(b => b.logintime).ToList();
                var a = list.Count;
                return Ok(new { code = 0, count = a, data = list, mes = "" });
            }
        }

        [HttpGet]
        public IActionResult Index(string we, string leixing)
        {

            string endid = "0";
            string str = "";
            while (endid != "")
            {
                we = we.Replace("https://webstatic.mihoyo.com/hk4e/event/e20190909gacha/index.html?", "https://hk4e-api.mihoyo.com/event/gacha_info/api/getGachaLog?");
                if (endid != "0")
                {
                    we = we.Substring(0, we.Length - 40);
                }
                we = we.Replace("game_biz=hk4e_cn#/log", "game_biz=hk4e_cn") + "&gacha_type=" + leixing + "&page=1&size=20&end_id=" + endid + "";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(we);
                WebResponse resp = req.GetResponse();
                StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.Default);
                string sReturn = sr.ReadToEnd().Trim();
                JObject result = JObject.Parse(sReturn);
                string data = result.Value<JToken>("data").ToString();
                JObject result2 = JObject.Parse(data);
                string replies = result2.Value<JToken>("list").ToString();
                if (replies == "[]")
                {
                    List<choukashuju> jsonq = JsonConvert.DeserializeObject<List<choukashuju>>("[" + str + "]");
                    return Ok(new { code = 0, count = jsonq.Count, data = jsonq, mes = "" });
                }
                List<choukashuju> ws = JsonConvert.DeserializeObject<List<choukashuju>>(replies);
                foreach (var item in ws)
                {
                    endid = item.id;
                }
                var datasw = replies.TrimStart("[".ToCharArray()).TrimEnd("]".ToCharArray());//去头去尾
                str += datasw.ToString() + ",";//拼接多个数组
                Thread.Sleep(300);
            }
            return Ok();
        }

    }
}
