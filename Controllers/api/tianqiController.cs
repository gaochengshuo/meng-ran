﻿using MengRan.Models.test;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MengRan.Controllers.api
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class tianqiController : Controller
    {
        [HttpGet]
        public IActionResult GetById(string demoReload)
        {
            string ip = "https://api.vvhan.com/api/weather?city=" + demoReload + "&type=week";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ip);
            WebResponse resp = req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.Default);
            string sReturn = sr.ReadToEnd().Trim();
            JObject result = JObject.Parse(sReturn);
            string data = result.Value<JToken>("data").ToString();
            //JObject ceshi = JObject.Parse(data);
            //string b = ceshi.Value<JToken>("forecast").ToString();
            List<tianqi> jsonq = JsonConvert.DeserializeObject<List<tianqi>>(data);
            return Ok(new { code = 0, count = jsonq.Count, data = jsonq, mes = "" });
        }
        
    }
}
