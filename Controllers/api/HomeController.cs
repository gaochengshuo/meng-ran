﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MengRan.Controllers.api
{
    [ApiController]
    [Route("Home/api/[Controller]/[action]")]
    public class HomeController : Controller
    {

        private IHostingEnvironment hostingEnv;
        public IActionResult de()
        {
            return View();
        }
        public HomeController(IHostingEnvironment env)
        {
            this.hostingEnv = env;
        }
        public IActionResult KindeditorPicUpload(IList<IFormFile> imgFile, string dir)
        {
            PicUploadResponse rspJson = new PicUploadResponse() { error = 0, url = "/upload/" };
            long size = 0;
            string tempname = "";
            foreach (var file in imgFile)
            {
                var filename = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');
                var extname = filename.Substring(filename.LastIndexOf("."), filename.Length - filename.LastIndexOf("."));
                var filename1 = System.Guid.NewGuid().ToString() + extname;
                tempname = filename1;
                var path = hostingEnv.WebRootPath;
                filename = hostingEnv.WebRootPath + $@"\upload\{filename1}";
                size += file.Length;
                using (FileStream fs = System.IO.File.Create(filename))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                    //这里是业务逻辑
                }
            }
            rspJson.error = 0;
            rspJson.url = $@"../../upload/" + tempname;
            return Json(rspJson);
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
    public class PicUploadResponse
    {
        public int error { get; set; }
        public string url { get; set; }
    }
}
