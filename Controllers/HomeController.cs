﻿
using MengRan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MengRan.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly MyDbContext _dbContext;
        public HomeController(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult chatmes()
        {
            return View();
        }
        public IActionResult index()
        {
            var list = _dbContext.men.ToList();
            return View(list);
        }
        public IActionResult pto()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult chatpeople()
        {
            return View();
        }
        public IActionResult liuyan()
        {
            return View();
        }
        public IActionResult yonghu()
        {
            return View();
        }
        public IActionResult ceshiye()
        {
            return View();
        }
        public IActionResult denglu()
        {
            return View();
        }
        public IActionResult tianqi()
        {
            return View();
        }
        public IActionResult bilimes()
        {
            return View();
        }
        public IActionResult bili()
        {
            return View();
        }
        public IActionResult bilis()
        {
            return View();
        }
        public IActionResult chouka()
        {
            return View();
        }
        public IActionResult mokuai()
        {
            var list = _dbContext.men.ToList();
            return View(list);
        }
        public IActionResult mens()
        {
            return View();       
        }
    }
}
