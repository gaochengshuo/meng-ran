﻿"use strict";

const connection = new signalR.HubConnectionBuilder()
    .withUrl("/chat")
    .build();
var uer = prompt("请给自己起个昵称");
//Disable send button until connection is established
//document.getElementById("sendButton").disabled = true;
//Cookie取值
function readCookie(name) {
    var cookieValue = "";
    var search = name + "=";
    if (document.cookie.length > 0) {
        var offset = document.cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            var end = document.cookie.indexOf(";", offset);
            if (end == -1)
                end = document.cookie.length;
            cookieValue = unescape(document.cookie.substring(offset, end))
        }
    }
    return cookieValue;
}
//Cookie设置值
function writeCookie(name, value, hours) {
    var expire = "";
    if (hours != null) {
        expire = new Date((new Date()).getTime() + hours * 3600000);
        expire = "; expires=" + expire.toGMTString();
    }
    document.cookie = name + "=" + escape(value) + expire;
}
//获取用户名 消息 时间，呈现在前台
connection.on("ReceiveMessage", function (user, message, time) {
    var msgs = document.getElementById("msgs");
    //判断用户在两侧的位置
    if (uer == user) {
        msgs.innerHTML = msgs.innerHTML + '<div class=\"msg guest\"><div class=\"msg-right\" worker=\"' + user + '\"><div class=\"msg-host photo\" style=\"background-image: url(../images/head.jpg)\"></div><div class=\"msg-ball\" title=\"' + time + '\">' + message + '</div></div></div>';
    } else {
        msgs.innerHTML = msgs.innerHTML + '<div class=\"msg guest\"><div class=\"msg-left\" worker=\"' + user + '\"><div class=\"msg-host photo\" style=\"background-image: url(../images/head.jpg)\"></div><div class=\"msg-ball\" title=\"' + time + '\">' + message + '</div></div></div>';
        var audio = new Audio("../mp3/chet.mp3");
        audio.play();


    }
});
function bofang() {
    var audio = new Audio("../mp3/chet.mp3");
    audio.play();
}
//获取用户列表数据，呈现在前台
connection.on("Connected", function (user, i, ConnectionID) {
    var msgs = document.getElementById("hots");

    if (i == 0) {
        msgs.innerHTML = "";
        msgs.innerHTML += '<li class=\"rel-item\" id="' + ConnectionID + '" onclick=\"Sendone(this)\">' + user + '</li>';
    } else {
        msgs.innerHTML += '<li class=\"rel-item\" id="' + ConnectionID + '" onclick=\"Sendone(this)\">' + user + '</li>';
    }
});
//连接时调用，将获得的uer用户名发送给集线器
connection.start().then(() => {
    connection.invoke('GetName', uer).catch(err => console.error(err.toString()))
}).catch(e => console.error(e.message))

//这段没什么意义，大致的意思就是按钮不被禁用
//connection.start().then(function () {
//    document.getElementById("submit").disabled = false;
//}).catch(function (err) {
//    return console.error(err.toString());
//});
//点击获取名字 消息发送集线器
function SendMsg() {
    var res = readCookie('asd');;
    let message = $(document.getElementsByTagName("iframe")[0].contentWindow.document.body).html();
    var name = uer;
    connection.invoke("SendMessage", name, message).catch(function (err) {
        return console.error(err.toString());
    });
    
    $.ajax({
        type: "get",
        contentType: "application/json",
        url: "api/chat/chatmes",
        data: { "use": name, "mes": message },
        success: function (message) {
            //alert(message);
        }
    })
    KindEditor.instances[0].html("") = "";
};
//一对一私聊
//function Sendone(e) {
//    var msgs = document.getElementById("msgs");
//    var res = e.id;
//    writeCookie('asd', null);
//    writeCookie('asd', res);
//    msgs.innerHTML = "";
//}

